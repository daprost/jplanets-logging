package de.jplanets.logging;

import de.jplanets.logging.log4j.Log4jLogger;

/**
 * This is the abstract Logger and the logging factory. There are following
 * levels defined:
 * <ul>
 * <li>trace</li>
 * <li>debug</li>
 * <li>info</li>
 * <li>hint</li>
 * <li>warn</li>
 * <li>error</li>
 * <li>fatal</li>
 * </ul>
 * The level trace is for logging method entering and exiting an can not be
 * accessed directly.
 * 
 * @author uli
 * @since 0.1.0
 */
public abstract class Logger {
  /**
   * The object seperator while formating.
   */
  public static final String COMMA = ","; //$NON-NLS-1$

  /**
   * The string ({@value}) that will be replaced by formating.
   */
  public static final String REPLACE = "$$"; //$NON-NLS-1$

  /**
   * The Prefix for entering of method.
   */
  public static final String IN_PRE = "entering "; //$NON-NLS-1$

  /**
   * The Prefix for exiting of method.
   */
  public static final String OUT_PRE = "exiting  "; //$NON-NLS-1$

  /**
   * The method opening (
   */
  public static final String METHOD_OPEN = "("; //$NON-NLS-1$

  /**
   * The method closing )
   */
  public static final String METHOD_CLOSE = ")"; //$NON-NLS-1$

  /**
   * Get an instance of a logger.
   * 
   * @param category the logger category
   * @return a Logger
   */
  public static Logger getLogger(final String category) {
    try {
      return new Log4jLogger(category);
    } catch (Throwable e) {
      // Empty catch. If not found use default.
    }
    return new SystemOutLogger(category);
  }

  /**
   * Log entering of method with level trace selfdetermining the method name
   * using {@link #getMethodName(String)}. See remarks there.
   * 
   * @param parameters The parameters to log.
   */
  public abstract void in(Object... parameters);

  /**
   * Log entering of method with level trace with the given method name.
   * 
   * @param method the method name.
   * @param parameters The parameters to log.
   */
  public abstract void in(String method, Object... parameters);

  /**
   * Log leaving of method with level trace selfdetermining the method name
   * using {@link #getMethodName(String)}. See remarks there.
   * 
   * @param parameters The parameters to log.
   */
  public abstract void out(Object... parameters);

  /**
   * Log leaving of method with level trace with the given method name.
   * 
   * @param method the method name.
   * @param parameters The parameters to log.
   */
  public abstract void out(String method, Object... parameters);

  /**
   * Log with debug level.
   * 
   * @param s The message string.
   * @param oa The objects to fill in the string.
   */
  public abstract void debug(String s, Object... oa);

  /**
   * Log with fatal level.
   * 
   * @param s The message string.
   * @param th The exception to log.
   * @param oa The objects to fill in the string.
   */
  public abstract void debug(String s, Throwable th, Object... oa);

  /**
   * Log with info level.
   * 
   * @param s The message string.
   * @param oa The objects to fill in the string.
   */
  public abstract void info(String s, Object... oa);

  /**
   * Log with fatal level.
   * 
   * @param s The message string.
   * @param th The exception to log.
   * @param oa The objects to fill in the string.
   */
  public abstract void info(String s, Throwable th, Object... oa);

  /**
   * Log with hint level.
   * 
   * @param s The message string.
   * @param oa The objects to fill in the string.
   */
  public abstract void hint(String s, Object... oa);

  /**
   * Log with fatal level.
   * 
   * @param s The message string.
   * @param th The exception to log.
   * @param oa The objects to fill in the string.
   */
  public abstract void hint(String s, Throwable th, Object... oa);

  /**
   * Log with warn level.
   * 
   * @param s The message string.
   * @param oa The objects to fill in the string.
   */
  public abstract void warn(String s, Object... oa);

  /**
   * Log with fatal level.
   * 
   * @param s The message string.
   * @param th The exception to log.
   * @param oa The objects to fill in the string.
   */
  public abstract void warn(String s, Throwable th, Object... oa);

  /**
   * Log with error level.
   * 
   * @param s The message string.
   * @param oa The objects to fill in the string.
   */
  public abstract void error(String s, Object... oa);

  /**
   * Log with fatal level.
   * 
   * @param s The message string.
   * @param th The exception to log.
   * @param oa The objects to fill in the string.
   */
  public abstract void error(String s, Throwable th, Object... oa);

  /**
   * Log with fatal level.
   * 
   * @param s The message string.
   * @param oa The objects to fill in the string.
   */
  public abstract void fatal(String s, Object... oa);

  /**
   * Log with fatal level.
   * 
   * @param s The message string.
   * @param th The exception to log.
   * @param oa The objects to fill in the string.
   */
  public abstract void fatal(String s, Throwable th, Object... oa);

  /**
   * Tell if the trace level is enabled for this logger.
   * 
   * @return {@code true} if trace is enabled.
   */
  public abstract boolean isTraceEnabled();

  /**
   * Tell if the debug level is enabled for this logger.
   * 
   * @return {@code true} if debug is enabled.
   */
  public abstract boolean isDebugEnabled();

  /**
   * Tell if the hint level is enabled for this logger.
   * 
   * @return {@code true} if hint is enabled.
   */
  public abstract boolean isHintEnabled();

  /**
   * Tell if the info level is enabled for this logger.
   * 
   * @return {@code true} if info is enabled.
   */
  public abstract boolean isInfoEnabled();

  /**
   * Tell if the warn level is enabled for this logger.
   * 
   * @return {@code true} if warn is enabled.
   */
  public abstract boolean isWarnEnabled();

  /**
   * Tell if the error level is enabled for this logger.
   * 
   * @return {@code true} if error is enabled.
   */
  public abstract boolean isErrorEnabled();

  /**
   * Tell if the fatal level is enabled for this logger.
   * 
   * @return {@code true} if fatal is enabled.
   */
  public abstract boolean isFatalEnabled();

  /**
   * Generate the list of parameters.
   * 
   * @param parameters the parameters.
   * @return the list of parameters.
   */
  protected String listParameters(Object... parameters) {
    Object[] objects = parameters;
    if (objects == null) {
      return ""; //$NON-NLS-1$
    }
    final StringBuffer result = new StringBuffer();
    int i = 0;
    for (; i < objects.length; i++) {
      result.append(COMMA + objects[i]);
    }
    return result.toString();
  
  }

  /**
   * Selfdetermining method name evaluation.
   * <p>
   * The current version based on java 1.4 this is ugly slow. So test the level
   * e.g. with {@link #isTraceEnabled()} before using it.
   * </p>
   * <p>
   * Im not shure if pre 1.4 java can determin the method name.
   * </p>
   * 
   * @param fqcn the full qualified class name of the calling Logger class.
   * @return the method just entered.
   */
  public static String getMethodName(final String fqcn) {
    final StackTraceElement[] stack = (new Throwable()).getStackTrace();
    StackTraceElement frame;
    String cname;
    // First, search back to a method in the fqcn class.
    int ix = 0;
    while (ix < stack.length) {
      frame = stack[ix];
      cname = frame.getClassName();
      if (cname.equals(fqcn)) {
        break;
      }
      ix++;
    }

    // Now search for the first frame before the "fqcn" class.
    while (ix < stack.length) {
      frame = stack[ix];
      cname = frame.getClassName();
      if (!cname.equals(fqcn)) {
        // We've found the relevant frame.
        // setSourceClassName(cname);
        return frame.getMethodName();
      }
      ix++;
    }
    return "NO METHOD FOUND";//$NON-NLS-1$
  }

  /**
   * Do the replacement of the {@value #REPLACE}s in the message with the objects. Failsafe
   * implementation. If objects are missing the $$ will be printed. If too many
   * objects they are appended comma seperated.
   * 
   * @param message The message to printout.
   * @param obj The objcects to insert.
   * @return the message with inserted values.
   */
  public static String format(final String message, final Object... obj) {
    Object[] objects = obj;
    if (objects == null) {
      objects = new Object[1];
    }
    final StringBuffer result;
    if (message == null) {
      result = new StringBuffer();
    } else {
      result = new StringBuffer(message);
    }
    int i = 0;
    int index = 0;
    for (; i < objects.length; i++) {
      index = result.indexOf(REPLACE);
      if (index >= 0) {
        result.delete(index, index + 2);
        result.insert(index, objects[i]);
      } else {
        for (; i < objects.length; i++) {
          result.append(COMMA + objects[i]);
        }
      }
    }
    return result.toString();
  }
}
