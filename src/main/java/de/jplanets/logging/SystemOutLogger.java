package de.jplanets.logging;

import java.io.PrintStream;

/**
 * This is a rather simple logger writing everything to System.out by default.
 * 
 * @author $Author$
 * @version $Revision$, $Date$
 */
// TODO currently this logger cannot be configured.
public class SystemOutLogger extends Logger {
  /**
   * Output of level FATAL
   */
  public static final String NAME_FATAL = "FATAL "; //$NON-NLS-1$

  /**
   * Output of level ERROR
   */
  public static final String NAME_ERROR = "ERROR "; //$NON-NLS-1$

  /**
   * Output of level WARN
   */
  private static final String NAME_WARN = "WARN  "; //$NON-NLS-1$

  /**
   * Output of level HINT
   */
  private static final String NAME_HINT = "HINT  "; //$NON-NLS-1$

  /**
   * Output of level INFO
   */
  private static final String NAME_INFO = "INFO  "; //$NON-NLS-1$

  /**
   * Output of level DEBUG
   */
  private static final String NAME_DEBUG = "DEBUG "; //$NON-NLS-1$

  /**
   * Output of level TRACE
   */
  private static final String NAME_TRACE = "TRACE "; //$NON-NLS-1$

  /**
   * Separator between message and exception message.
   */
  public static final String EXCEPTION_MESSAGE_SEPERATOR = " : "; //$NON-NLS-1$

  /**
   * Seperator used to devide category output from message.
   */
  public static final String CATEGORY_SEPERATOR = " "; //$NON-NLS-1$

  /** The output ot redirect to. */
  private static PrintStream _out = System.out;

  /** The category of this logger. */
  private String _category;

  /** flag for the log level. */
  private boolean _trace = true;

  /** flag for the log level. */
  private boolean _debug = true;

  /** flag for the log level. */
  private boolean _hint = true;

  /** flag for the log level. */
  private boolean _info = true;

  /** flag for the log level. */
  private boolean _warn = true;

  /** flag for the log level. */
  private boolean _error = true;

  /** flag for the log level. */
  private boolean _fatal = true;

  /**
   * Creates a new instance of SystemOutLogger.
   * 
   * @param loggerCategory the logger category
   */
  public SystemOutLogger(final String loggerCategory) {
    this._category = loggerCategory;
  }

  /**
   * {@inheritDoc} Tests if level trace is on before detemining method name.
   */
  @Override
  public void in(Object... parameters) {
    if (!isTraceEnabled()) {
      return;
    }
    in(getMethodName(this.getClass().getName()), parameters);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void in(final String method, Object... parameters) {
    outputMethod(NAME_TRACE, method, IN_PRE, parameters);
  }

  /**
   * {@inheritDoc} Tests if level trace is on before detemining method name.
   */
  @Override
  public void out(Object... parameters) {
    if (!isTraceEnabled()) {
      return;
    }
    out(getMethodName(this.getClass().getName()), parameters);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void out(final String method, Object... parameters) {
    outputMethod(NAME_TRACE, method, OUT_PRE, parameters);
  }

  /**
   * Generate output for method logging.
   * 
   * @param level the level to log with.
   * @param method the method name.
   * @param direction entering or exiting.
   * @param parameters the parameters to fill in.
   */
  private void outputMethod(final String level, final String method,
      final String direction, final Object... parameters) {
    _out.println(_category + CATEGORY_SEPERATOR + level + direction + method
        + METHOD_OPEN + listParameters(parameters) + METHOD_CLOSE);

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void debug(final String message, final Object... fillIns) {
    output(NAME_DEBUG, message, null, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void debug(final String message, final Throwable th,
      final Object... fillIns) {
    output(NAME_DEBUG, message, th, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void info(final String message, Object... fillIns) {
    output(NAME_INFO, message, null, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void info(final String message, Throwable th, Object... fillIns) {
    output(NAME_INFO, message, th, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void hint(final String message, Object... fillIns) {
    output(NAME_HINT, message, null, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void hint(final String message, Throwable th, Object... fillIns) {
    output(NAME_HINT, message, th, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void warn(final String message, Object... fillIns) {
    output(NAME_WARN, message, null, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void warn(final String message, Throwable th, Object... fillIns) {
    output(NAME_WARN, message, th, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void error(final String message, Object... fillIns) {
    output(NAME_ERROR, message, null, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void error(final String message, Throwable th, Object... fillIns) {
    output(NAME_ERROR, message, th, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void fatal(final String message, Object... fillIns) {
    output(NAME_FATAL, message, null, fillIns);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void fatal(final String message, Throwable th, Object... fillIns) {
    output(NAME_FATAL, message, th, fillIns);
  }

  /**
   * Do the output and formating.
   * 
   * @param level the level.
   * @param message the message to format.
   * @param th the exception to log.
   * @param fillIns the fill ins for the message.
   */
  private void output(final String level, final String message,
      final Throwable th, final Object... fillIns) {
    if (th == null) {
      _out.println(_category + CATEGORY_SEPERATOR + level
          + Logger.format(message, fillIns));
    } else {
      _out.println(_category + CATEGORY_SEPERATOR + level
          + Logger.format(message, fillIns) + EXCEPTION_MESSAGE_SEPERATOR
          + th.getMessage());
    }
    if (th != null) {
      th.printStackTrace(_out);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isTraceEnabled() {
    return _trace;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDebugEnabled() {
    return _debug;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isInfoEnabled() {
    return _info;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isHintEnabled() {
    return _hint;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isWarnEnabled() {
    return _warn;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isErrorEnabled() {
    return _error;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isFatalEnabled() {
    return _fatal;
  }

  /**
   * Switch to another output.
   * 
   * @param out the output to use.
   */
  public void setOut(PrintStream out) {
    _out = out;
  }
}
