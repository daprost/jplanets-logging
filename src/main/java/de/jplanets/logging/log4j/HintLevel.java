package de.jplanets.logging.log4j;

import java.io.Serializable;

import org.apache.log4j.Level;

/**
 * Log level hint. Extension of the Level with hint.
 * 
 * <pre>
 *  &lt;category name=”org.myapp”&gt;            
 *  &lt;priority value=”MY_TRACE” class=”org.myapp.log.MyTraceLevel” /&gt;            
 *  &lt;appender-ref ref=”FILE”/&gt;            
 *  &lt;/category&gt;
 * 
 *  log4j.rootLogger=trace#my.package.TraceLevel, MAIN, STDERR
 * </pre>
 * 
 * @author uli
 */
public class HintLevel extends Level implements Serializable {
  /**
   * Serialization version ID.
   */
  static final long serialVersionUID = 5L;

  /**
   * The String representation of the HINTLevel
   */
  public static final String HINT_NAME = "HINT"; //$NON-NLS-1$

  /**
   * The int representation of the level
   */
  public static final int HINT_INT = 25000;

  /**
   * The Syslog equivalent
   */
  private static final int SYSLOG_TRACE_INT = 5;

  /**
   * The HintLevel as a constang
   */
  public static final HintLevel HINT = new HintLevel(HINT_INT, HINT_NAME,
      SYSLOG_TRACE_INT);

  /**
   * See log4j doku.
   * 
   * @param level int level
   * @param strLevel leven name
   * @param syslogEquiv level in syslog
   */
  protected HintLevel(int level, String strLevel, int syslogEquiv) {
    super(level, strLevel, syslogEquiv);
  }

  /**
   * log4j doku: Convert the string passed as argument to a level. If the
   * conversion fails, then this method returns {@link #DEBUG}.
   * 
   * @param s the string.
   * @return the level.
   */
  public static Level toLevel(String s) {
    if (HINT_NAME.equalsIgnoreCase(s)) {
      return HINT;
    }
    return Level.toLevel(s);
  }

  /**
   * log4j doku: Convert an integer passed as argument to a level. If the
   * conversion fails, then this method returns {@link #DEBUG}.
   * 
   * @param i the int.
   * @return the level.
   */
  public static Level toLevel(int i) {
    if (i == HINT_INT) {
      return HINT;
    }
    return Level.toLevel(i);
  }

  /**
   * log4j doku: Convert an integer passed as argument to a level. If the
   * conversion fails, then this method returns the specified default.
   * 
   * @param i the int.
   * @param level the level.
   * @return the level.
   */
  public static Level toLevel(int i, Level level) {
    if (i == HINT_INT) {
      return HINT;
    }
    return Level.toLevel(i, level);
  }

  /**
   * see log4j doku: Convert the string passed as argument to a level. If the
   * conversion fails, then this method returns the value of
   * <code>defaultLevel</code>.
   * 
   * @param s the string.
   * @param level the level.
   * @return the level.
   */
  public static Level toLevel(String s, Level level) {
    if (HINT_NAME.equalsIgnoreCase(s)) {
      return HINT;
    }
    return Level.toLevel(s, level);
  }

}
