package de.jplanets.logging.log4j;

import org.apache.log4j.Level;

import de.jplanets.logging.Logger;

/**
 * This is a Logger using log4j.
 * 
 * @author uli
 * @version $Revision$, $Date$
 */

public class Log4jLogger extends de.jplanets.logging.Logger {
  /**
   * the internal category.
   */
  private org.apache.log4j.Logger _log;

  /**
   * Create a logger.
   * 
   * @param category the logging category.
   */
  public Log4jLogger(final String category) {
    this._log = org.apache.log4j.Logger.getLogger(category);
  }
  
  /**
   * {@inheritDoc} Tests if level trace is on before detemining method name.
   */
  @Override
  public void in(Object... parameters) {
    if (!isTraceEnabled()) {
      return;
    }
    in(getMethodName(this.getClass().getName()), parameters);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void in(final String method, Object... parameters) {
    _log.trace(IN_PRE + method + METHOD_OPEN + listParameters(parameters)+METHOD_CLOSE);
  }

  /**
   * {@inheritDoc} Tests if level trace is on before detemining method name.
   */
  @Override
  public void out(Object... parameters) {
    if (!isTraceEnabled()) {
      return;
    }
    out(getMethodName(this.getClass().getName()), parameters);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void out(final String method, Object... parameters) {
    _log.trace(OUT_PRE + method + METHOD_OPEN + listParameters(parameters)+METHOD_CLOSE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void debug(final String message, final Object... fillIns) {
    _log.debug(Logger.format(message, fillIns));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void debug(final String message, final Throwable th,
      final Object... fillIns) {
    _log.debug(Logger.format(message, fillIns),th);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void info(final String message, Object... fillIns) {
    _log.info(Logger.format(message, fillIns));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void info(final String message, Throwable th, Object... fillIns) {
    _log.info(Logger.format(message, fillIns),th);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void hint(final String message, Object... fillIns) {
    _log.log(HintLevel.HINT, Logger.format(message, fillIns));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void hint(final String message, Throwable th, Object... fillIns) {
    _log.log(HintLevel.HINT, Logger.format(message, fillIns),th);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void warn(final String message, Object... fillIns) {
    _log.warn(Logger.format(message, fillIns));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void warn(final String message, Throwable th, Object... fillIns) {
    _log.warn(Logger.format(message, fillIns),th);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void error(final String message, Object... fillIns) {
    _log.error(Logger.format(message, fillIns));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void error(final String message, Throwable th, Object... fillIns) {
    _log.error(Logger.format(message, fillIns),th);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void fatal(final String message, Object... fillIns) {
    _log.fatal(Logger.format(message, fillIns));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void fatal(final String message, Throwable th, Object... fillIns) {
    _log.fatal(Logger.format(message, fillIns),th);
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isTraceEnabled() {
    return _log.isTraceEnabled();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDebugEnabled() {
    return _log.isDebugEnabled();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isInfoEnabled() {
    return _log.isInfoEnabled();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isHintEnabled() {
    return _log.isEnabledFor(HintLevel.HINT);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isWarnEnabled() {
    return _log.isEnabledFor(Level.WARN);
  }
  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isErrorEnabled() {
    return _log.isEnabledFor(Level.ERROR);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isFatalEnabled() {
    return _log.isEnabledFor(Level.FATAL);
  }



}
