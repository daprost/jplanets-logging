package de.jplanets.logging;

import java.io.ByteArrayOutputStream;

import junit.framework.TestCase;

/**
 * Test the SystemOutLogger.
 * 
 * @author uli
 */
public abstract class AbstractLoggerTest extends TestCase {
  /**
   * Test the Logger
   * 
   * @param name The Test name
   */
  public AbstractLoggerTest(String name) {
    super(name);
  }
  /**
   * The internal logger to use in the tests.
   */
  public Logger log = null;

  /**
   * The output the logger writes to.
   */
  public ByteArrayOutputStream out = new ByteArrayOutputStream();

  /**
   * The category to use for the Loggers.
   */
  public static final String CATEGORY = AbstractLoggerTest.class.getName();
  /**
   * Test the selfdetermining method entering logging.
   */
  public void testInDynamic() {
    log.in();
    assertEquals(
        CATEGORY + " TRACE entering testInDynamic()"+System.lineSeparator(), out.toString()); //$NON-NLS-1$
  }

  /**
   * Test the method entering logging with given name.
   */
  public void testInStatic() {
    log.in("method"); //$NON-NLS-1$
    assertEquals(
        CATEGORY + " TRACE entering method()"+System.lineSeparator(), out.toString()); //$NON-NLS-1$
  }

  /**
   * Test the selfdetermining method exeting logging.
   */
  public void testOutDynamic() {
    log.out();
    assertEquals(
        CATEGORY + " TRACE exiting  testOutDynamic()"+System.lineSeparator(), out.toString()); //$NON-NLS-1$
  }

  /**
   * Test the method exiting logging with given name.
   */
  public void testOutStatic() {
    log.out("method"); //$NON-NLS-1$
    assertEquals(
        CATEGORY + " TRACE exiting  method()"+System.lineSeparator(), out.toString()); //$NON-NLS-1$
  }

  /** 
   * test all levels at standard output
   */
  public void testAllLevels() {
    log.debug("Test");//$NON-NLS-1$
    assertEquals(CATEGORY + " DEBUG Test"+System.lineSeparator(),out.toString());//$NON-NLS-1$
    out.reset();
    log.info("Test");//$NON-NLS-1$
    assertEquals(CATEGORY + " INFO  Test"+System.lineSeparator(),out.toString());//$NON-NLS-1$
    out.reset();
    log.hint("Test");//$NON-NLS-1$
    assertEquals(CATEGORY + " HINT  Test"+System.lineSeparator(),out.toString());//$NON-NLS-1$
    out.reset();
    log.error("Test");//$NON-NLS-1$
    assertEquals(CATEGORY + " ERROR Test"+System.lineSeparator(),out.toString());//$NON-NLS-1$
    out.reset();
    log.fatal("Test");//$NON-NLS-1$
    assertEquals(CATEGORY + " FATAL Test"+System.lineSeparator(),out.toString());//$NON-NLS-1$
    out.reset();
  }
  
}
