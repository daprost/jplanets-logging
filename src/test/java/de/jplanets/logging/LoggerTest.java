package de.jplanets.logging;

import junit.framework.TestCase;

/**
 * Test the Logger.
 * 
 * @author uli
 */
public class LoggerTest extends TestCase {

  /**
   * Test the Logger
   * 
   * @param name The Test name
   */
  public LoggerTest(String name) {
    super(name);
  }

  /**
   * Test the selfdetemining method name.
   */
  public void testMethodDetermin() {
    String name = Logger.getMethodName(Logger.class.getName());
    assertEquals("testMethodDetermin", name); //$NON-NLS-1$
  }
  /**
   * TODO implement the tests
   *
   */
  public void testListParameters() {
  // test
  }
  /**
   * Test the format method
   *
   */
  public void testFormat() {
    // no replacement
    assertEquals("Uli", Logger.format("Uli"));//$NON-NLS-1$//$NON-NLS-2$
    // no replacement with $$
    assertEquals("Uli $$", Logger.format("Uli $$"));//$NON-NLS-1$//$NON-NLS-2$
    assertEquals("Uli $$ $$", Logger.format("Uli $$ $$"));//$NON-NLS-1$//$NON-NLS-2$
    // single replacement
    assertEquals("Uli hat", Logger.format("Uli $$", "hat"));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    // no $$ but objects
    assertEquals("Uli,hat", Logger.format("Uli","hat"));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    assertEquals("Uli,hat,auch", Logger.format("Uli","hat","auch"));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$//$NON-NLS-4$
    // null appending
    assertEquals("Uli,null", Logger.format("Uli",(Object[])null));//$NON-NLS-1$//$NON-NLS-2$
    assertEquals("Uli,null,null", Logger.format("Uli",null,null));//$NON-NLS-1$//$NON-NLS-2$
    assertEquals("Uli,null,hat,null", Logger.format("Uli",null,"hat",null));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    // null messages
    assertEquals("", Logger.format(null));//$NON-NLS-1$
    assertEquals(",Uli", Logger.format(null, "Uli"));//$NON-NLS-1$//$NON-NLS-2$
    assertEquals(",Uli,hat", Logger.format(null, "Uli","hat"));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    // null replacement
    assertEquals("Uli null", Logger.format("Uli $$",(Object[])null));//$NON-NLS-1$//$NON-NLS-2$
    assertEquals("Uli null ,null", Logger.format("Uli $$ ",null,null));//$NON-NLS-1$//$NON-NLS-2$
    assertEquals("Uli null hat,null", Logger.format("Uli $$ $$",null,"hat",null));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    assertEquals("Uli null hat null ", Logger.format("Uli $$ $$ $$ ",null,"hat",null));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    assertEquals("Uli null hat null $$", Logger.format("Uli $$ $$ $$ $$",null,"hat",null));//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    

  }
}
