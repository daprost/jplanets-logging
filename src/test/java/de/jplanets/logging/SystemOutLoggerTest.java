package de.jplanets.logging;

import java.io.PrintStream;

/**
 * Test the SystemOutLogger.
 * 
 * @author uli
 */
public class SystemOutLoggerTest extends AbstractLoggerTest {
  /**
   * Test the Logger
   * 
   * @param name The Test name
   */
  public SystemOutLoggerTest(String name) {
    super(name);
  }

  /**
   * test the internal output method.
   *
   */
  public void testOutput() {
    log.debug("Hallo");//$NON-NLS-1$
    assertEquals(CATEGORY + " DEBUG Hallo"+System.lineSeparator(),out.toString());//$NON-NLS-1$
    out.reset();
    Throwable th = new Throwable("Test");//$NON-NLS-1$
    log.debug("Hallo $$ $$",th, "uli", "two");//$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
    String s = out.toString();
    s = s.substring(0,s.indexOf("\n")+1);//$NON-NLS-1$
    assertEquals(CATEGORY + " DEBUG Hallo uli two : Test"+System.lineSeparator(),s);//$NON-NLS-1$
    out.reset();
  }

  @Override
  protected void setUp() throws Exception {
    log = new SystemOutLogger(CATEGORY);
    ((SystemOutLogger)log).setOut(new PrintStream(out));
    super.setUp();
  }

  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
  }

}
