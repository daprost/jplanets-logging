/**
 * 
 */
package de.jplanets.logging.log4j;

import java.io.PrintWriter;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;

import de.jplanets.logging.AbstractLoggerTest;

/**
 * Test the Log4JLogger implementation.
 * 
 * @author uli
 */
public class Log4JLoggerTest extends AbstractLoggerTest {
  /**
   * the appender used for testing.
   */
  static WriterAppender w = new WriterAppender();
  /**
   * initialize Log4J
   */
  static {
    PatternLayout layout = new PatternLayout("%c %-5p %m%n");//$NON-NLS-1$
    PatternLayout layout2 = new PatternLayout("%d %c %-5p %m%n");//$NON-NLS-1$
    Logger logger = Logger.getRootLogger();
    logger.setLevel(Level.ALL);
    w.setLayout(layout);
    logger.addAppender(w);
    logger.addAppender(new ConsoleAppender(layout2));
    
  }
  /**
   * Test the Logger
   * 
   * @param name The Test name
   */
  public Log4JLoggerTest(String name) {
    super(name);
    
  }
  /*
   * (non-Javadoc)
   * 
   * @see junit.framework.TestCase#setUp()
   */
  @Override
  protected void setUp() throws Exception {
    log = new Log4jLogger(CATEGORY);
    w.setWriter(new PrintWriter(out));
    super.setUp();
  }

}
